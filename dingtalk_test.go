package cob

import (
	"fmt"
	"testing"
)

func Test(t *testing.T) {
	// https://oapi.dingtalk.com/robot/send?access_token=59ccb3667364144a8b38bd2b2c3d063f79580da942c38527b273a0671e288ad7
	// SEC74ec6d5c1c1debeb22f4f567b6a8168bf6f78133e35a0b9d6b0b8487cd990122

	obj := NewDingTalk("59ccb3667364144a8b38bd2b2c3d063f79580da942c38527b273a0671e288ad7", "SEC74ec6d5c1c1debeb22f4f567b6a8168bf6f78133e35a0b9d6b0b8487cd990122")
	err := obj.SendMsgToDingDing("test")
	fmt.Println(err)
}
