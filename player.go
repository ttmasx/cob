package cob

import (
	"embed"
	"github.com/faiface/beep"
	"github.com/faiface/beep/mp3"
	"github.com/faiface/beep/speaker"
	"log"
	"os"
	"sync"
	"time"
)

type Player struct {
	AudioPath  []string
	AudioIndex map[string][]int
	Buffer     *beep.Buffer
}

type Music struct {
	*beep.Ctrl
}

func (m *Music) Stop() {
	m.Paused = true
}

func (m *Music) Start() {
	m.Paused = false
}

// NewPlayerByEmbed 通过嵌入mp3文件的方式初始化播放器
func NewPlayerByEmbed(e embed.FS, AudioPath []string) *Player {
	var once sync.Once
	var myBuffer *beep.Buffer
	AudioIndex := make(map[string][]int)
	for _, path := range AudioPath {
		f, err := e.Open(path)
		if err != nil {
			log.Fatal(err)
		}

		streamer, format, err := mp3.Decode(f)
		if err != nil {
			log.Fatal(err)
		}
		once.Do(func() {
			speaker.Init(format.SampleRate, format.SampleRate.N(time.Second/10))
			myBuffer = beep.NewBuffer(format)
		})
		// TODO
		tmpIdx := []int{myBuffer.Len(), myBuffer.Len() + streamer.Len()}
		AudioIndex[path] = tmpIdx

		myBuffer.Append(streamer)
		streamer.Close()
	}

	return &Player{
		AudioPath:  AudioPath,
		AudioIndex: AudioIndex,
		Buffer:     myBuffer,
	}
}

func NewPlayer(AudioPath []string) *Player {
	var once sync.Once
	var myBuffer *beep.Buffer
	AudioIndex := make(map[string][]int)
	for _, path := range AudioPath {
		f, err := os.Open(path)
		if err != nil {
			log.Fatal(err)
		}

		streamer, format, err := mp3.Decode(f)
		if err != nil {
			log.Fatal(err)
		}
		once.Do(func() {
			speaker.Init(format.SampleRate, format.SampleRate.N(time.Second/10))
			myBuffer = beep.NewBuffer(format)
		})
		// TODO
		tmpIdx := []int{myBuffer.Len(), myBuffer.Len() + streamer.Len()}
		AudioIndex[path] = tmpIdx

		myBuffer.Append(streamer)
		streamer.Close()
	}

	return &Player{
		AudioPath:  AudioPath,
		AudioIndex: AudioIndex,
		Buffer:     myBuffer,
	}
}

// Play 播放音乐，非阻塞
func (p *Player) Play(name string) Music {
	shot := p.Buffer.Streamer(p.AudioIndex[name][0], p.AudioIndex[name][1])
	ctrl := &beep.Ctrl{
		Streamer: shot,
		Paused:   false,
	}
	speaker.Play(ctrl)
	return Music{ctrl}
}
