package rand

import (
	"fmt"
	"testing"
)

func TestGenerateRandomString(t *testing.T) {
	for i := 0; i < 10; i++ {
		fmt.Println(GenerateRandomString(100, DIGIT|LOWER))
	}
}

//go test -bench=BenchmarkGenerateRandomString* -benchmem
//BenchmarkGenerateRandomString1-20        2500927               478.1 ns/op           416 B/op          5 allocs/op
//BenchmarkGenerateRandomString2-20        3147523               385.2 ns/op           224 B/op          2 allocs/op

func BenchmarkGenerateRandomString1(b *testing.B) {
	for i := 0; i < b.N; i++ {
		_, err := GenerateRandomString(100, DIGIT|LOWER|UPPER|SPECIAL)
		if err != nil {
			b.Fatal(err)
		}
	}
}

func BenchmarkGenerateRandomString2(b *testing.B) {
	randStr := NewRandStr(DigitCharacters + LowercaseCharacters + UppercaseCharacters + SpecialCharacters)
	for i := 0; i < b.N; i++ {
		_, err := randStr.GenerateRandomString(100)
		if err != nil {
			b.Fatal(err)
		}
	}
}
