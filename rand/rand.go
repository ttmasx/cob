package rand

import (
	"crypto/rand"
	"errors"
	"unsafe"
)

const (
	DigitCharacters     = "0123456789"
	LowercaseCharacters = "abcdefghijklmnopqrstuvwxyz"
	UppercaseCharacters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
	SpecialCharacters   = "!@#$%^&*()"
)

const (
	DIGIT = 1 << iota
	LOWER
	UPPER
	SPECIAL
)

type RandStr struct {
	Charset string
}

func NewRandStr(charset string) *RandStr {
	return &RandStr{Charset: charset}
}

func (r *RandStr) GenerateRandomString(n int) (string, error) {
	if len(r.Charset) == 0 {
		return "", errors.New("charset cannot be empty")
	}
	bytes := make([]byte, n)
	if _, err := rand.Read(bytes); err != nil {
		return "", err
	}
	for i, b := range bytes {
		bytes[i] = r.Charset[b%byte(len(r.Charset))]
	}
	//return string(bytes), nil
	//return *(*string)(unsafe.Pointer(&bytes)), nil
	return Bytes2String(bytes), nil
}

func Bytes2String(b []byte) string {
	return *(*string)(unsafe.Pointer(&b))
}

func GenerateRandomString(n int, flag int, OptCharset ...string) (string, error) {
	var charset string
	if flag&DIGIT != 0 {
		charset += DigitCharacters
	}
	if flag&LOWER != 0 {
		charset += LowercaseCharacters
	}
	if flag&UPPER != 0 {
		charset += UppercaseCharacters
	}
	if flag&SPECIAL != 0 {
		charset += SpecialCharacters
	}
	for _, v := range OptCharset {
		charset += v
	}

	return NewRandStr(charset).GenerateRandomString(n)
}
