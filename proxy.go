package cob

import (
	"github.com/gin-gonic/gin"
	"io"
	"log"
	"net/http"
	"net/url"
	"sync"
	"time"
)

type Proxy struct{}

var DProxy = &Proxy{}

// TestMulHttpProxy 批量测试多个HTTP代理
func (obj *Proxy) TestMulHttpProxy(proxys []string, timeout int, maxG int, printLog bool) (success []string, fail []string) {
	var wg sync.WaitGroup
	var mu sync.Mutex
	ch := make(chan bool, maxG)
	for _, proxy := range proxys {
		wg.Add(1)
		ch <- true
		go func(proxy string) {
			t := time.Now()
			_, err := obj.TestOneHttpProxy(proxy, timeout)
			if err != nil {
				if printLog {
					log.Println(proxy, "fail", err)
				}
				mu.Lock()
				fail = append(fail, proxy)
				mu.Unlock()
			} else {
				if printLog {
					log.Println(proxy, "success", time.Since(t))
				}
				mu.Lock()
				success = append(success, proxy)
				mu.Unlock()
			}
			wg.Done()
			<-ch
		}(proxy)
	}
	wg.Wait()
	return
}

// TestOneHttpProxy 测试单个http代理是否可用
func (obj *Proxy) TestOneHttpProxy(proxy string, timeout int) (string, error) {
	client, err := obj.CreateHttpProxyClient(proxy, timeout)
	if err != nil {
		return "", err
	}
	resp, err := client.Get("https://ip.sb/")
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()
	b, err := io.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}
	return string(b), nil
}

// CreateHttpProxyClient 创建一个http代理客户端，代理类型由uri确定，支持http/https/socks5，默认为http
// http://fans007:fans888@45.76.169.156:39000
func (obj *Proxy) CreateHttpProxyClient(proxy string, timeout int) (*http.Client, error) {
	u, err := url.Parse(proxy)
	if err != nil {
		return nil, err
	}
	return &http.Client{
		Transport: &http.Transport{Proxy: http.ProxyURL(u)},
		Timeout:   time.Duration(timeout) * time.Second,
	}, nil
}

// CreateFileServerByGin 创建一个HTTP文件服务器，通过gin框架
func (obj *Proxy) CreateFileServerByGin(localPath string, relativePath string, port string, OnlyListenLocalAddr bool) {
	router := gin.Default()
	router.StaticFS(relativePath, http.Dir(localPath))
	if OnlyListenLocalAddr {
		router.Run("127.0.0.1:" + port)
	} else {
		router.Run(":" + port)
	}
}

// CreateFileServer 创建一个HTTP文件服务器，注意relativePath路径后要加斜杠
// example:DNepal.CreateFileServer("C:\\lee\\project\\go\\auto", "/static/", "8888", true)
func (obj *Proxy) CreateFileServer(localPath string, relativePath string, port string, OnlyListenLocalAddr bool) {
	fs := http.FileServer(http.Dir(localPath))
	http.Handle(relativePath, http.StripPrefix(relativePath, fs))
	if OnlyListenLocalAddr {
		log.Printf("Listening and serving HTTP on 127.0.0.1:%s\n", port)
		http.ListenAndServe("127.0.0.1:"+port, nil)
	} else {
		log.Printf("Listening and serving HTTP on 0.0.0.0:%s\n", port)
		http.ListenAndServe(":"+port, nil)
	}
}
