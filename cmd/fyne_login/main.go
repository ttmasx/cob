package main

import (
	"fmt"
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/app"
	"fyne.io/fyne/v2/canvas"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/theme"
	"fyne.io/fyne/v2/widget"
	"gitee.com/ttmasx/cob/mytheme"
	"log"
	"net/url"
	"time"
)

// 二维码怎么实现点击？

func IsDuplicateSubmission(m map[string]time.Time, inputStr string) bool {
	if t,ok := m[inputStr];ok{
		if time.Now().Add(-time.Second*3).Before(t) {
			return true
		}
	}
	m[inputStr] = time.Now()
	return false
}

func main() {
	mytheme.InitFont(mytheme.Dengb)		// 设置中文字体
	myApp := app.New()
	myApp.Settings().SetTheme(mytheme.NewMyTheme(nil, mytheme.Gray))		// 设置主题，自定义颜色

	var windowW,windowH float32 = 430,300
	myWindow := myApp.NewWindow("QQ")
	myWindow.Resize(fyne.NewSize(windowW, windowH))
	myWindow.CenterOnScreen()					// 窗口居中
	myWindow.SetFixedSize(true)					// 窗口锁定大小
	myWindow.SetIcon(mytheme.ResourceQQSvg)		// 窗口图标

	userLabel := canvas.NewImageFromResource(theme.AccountIcon())
	passLabel := canvas.NewImageFromResource(mytheme.ResourceLockSvg)
	userInput := widget.NewEntry()
	passInput := widget.NewEntry()
	link, err := url.Parse("https://www.baidu.com/")
	if err != nil {
		log.Fatalln(err)
	}
	register := widget.NewHyperlink("注册账号", link)
	quickMark := canvas.NewImageFromResource(mytheme.ResourceQuickMarkSvg)

	userInput.TextStyle = fyne.TextStyle{Bold:true}
	passInput.TextStyle = fyne.TextStyle{Bold:true}
	passInput.Password = true
	userInput.SetPlaceHolder("账号")
	passInput.SetPlaceHolder("密码")
	var loginButton *widget.Button
	m := make(map[string]time.Time)		// 防止短时间内重复提交
	loginButton = widget.NewButton("登录", func() {
		loginButton.Disable()
		defer loginButton.Enable()

		if userInput.Text == "" || passInput.Text == "" {
			log.Println("请勿提交空值")
			return
		}

		if IsDuplicateSubmission(m, userInput.Text+passInput.Text) {
			log.Println("请勿重复提交")
			return
		}
		// 执行业务逻辑
		time.Sleep(time.Second*1)
		fmt.Println(userInput.Text, passInput.Text)
	})
	loginButton.Icon = theme.LoginIcon()

	var h float32 = 30									// 所有组件高度,同时代表图标的长度(高必须大于等于30,否则出现滚动条)
	var entryLen float32 = 230							// 输入框长度
	var interval float32 = 10							// 图标与输入框的间隔
	var y float32 = (windowH - interval*2 - h*3)*2/3	//	userLabel坐标，居2/3
	var x float32 = (windowW-h-entryLen-interval)/2		//	userLabel坐标，居中

	userLabel.Move(fyne.NewPos(x, y))
	passLabel.Move(fyne.NewPos(x, y+h+interval))

	userInput.Move(fyne.NewPos(x+h+interval, y))
	passInput.Move(fyne.NewPos(x+h+interval, y+h+interval))

	userLabel.Resize(fyne.NewSize(h,h))
	passLabel.Resize(fyne.NewSize(h,h))
	userInput.Resize(fyne.NewSize(entryLen,h))
	passInput.Resize(fyne.NewSize(entryLen,h))

	loginButton.Move(fyne.NewPos(x,y+(h+interval)*2))
	loginButton.Resize(fyne.NewSize(entryLen+interval+h, h))

	register.Move(fyne.NewPos(0, windowH-h-interval))
	register.Resize(fyne.NewSize(68,h))		// 字体占用的宽度

	quickMark.Move(fyne.NewPos(windowW-h-interval, windowH-h-interval))
	quickMark.Resize(fyne.NewSize(h,h))

	c := container.NewWithoutLayout(userLabel, passLabel, userInput, passInput, loginButton, register, quickMark)

	myWindow.SetContent(c)
	myWindow.ShowAndRun()
}
