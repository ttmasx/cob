package chatgpt

import (
	"bytes"
	"encoding/json"
	"gitee.com/ttmasx/cob"
	"io"
	"log"
	"net/http"
)

type ChatAPI struct {
	Key    string
	Proxy  string
	Client *http.Client
}

func NewChatAPI(key, proxyUrl string) *ChatAPI {
	var c *http.Client
	var err error
	if proxyUrl != "" {
		c, err = cob.DProxy.CreateHttpProxyClient(proxyUrl, 60)
		if err != nil {
			log.Fatalln(err)
		}
	} else {
		c = &http.Client{}
	}
	return &ChatAPI{
		Key:    key,
		Proxy:  proxyUrl,
		Client: c,
	}
}

func (obj *ChatAPI) Ask(question string) string {
	param := CreateChatCompletionStructParam{
		Model:    "gpt-3.5-turbo",
		Messages: nil,
	}
	param.Messages = append(param.Messages, struct {
		Role    string `json:"role"`
		Content string `json:"content"`
	}{Role: "user", Content: question})

	r, err := obj.CreateChatCompletion(param)
	if err != nil {
		return err.Error()
	}
	var answer string
	for _, v := range r.Choices {
		answer += v.Message.Content
	}
	return answer
}

// CreateChatCompletion 创建一个会话
func (obj *ChatAPI) CreateChatCompletion(param CreateChatCompletionStructParam) (*CreateChatCompletionStruct, error) {
	b, err := json.Marshal(param)
	if err != nil {
		log.Println(err)
		return nil, err
	}
	req, err := http.NewRequest("POST", "https://api.openai.com/v1/chat/completions", bytes.NewReader(b))
	if err != nil {
		log.Println(err)
		return nil, err
	}
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", "Bearer "+obj.Key)
	resp, err := obj.Client.Do(req)
	if err != nil {
		log.Println(err)
		return nil, err
	}
	defer resp.Body.Close()
	bodyText, err := io.ReadAll(resp.Body)
	if err != nil {
		log.Println(err)
		return nil, err
	}
	var v CreateChatCompletionStruct
	err = json.Unmarshal(bodyText, &v)
	if err != nil {
		log.Println(err)
		return nil, err
	}
	return &v, nil
}

// CreateImage 生成一张图片
func (obj *ChatAPI) CreateImage(param CreateImageStructParam) (*CreateImageStruct, error) {
	b, err := json.Marshal(param)
	if err != nil {
		return nil, err
	}
	req, err := http.NewRequest("POST", "https://api.openai.com/v1/images/generations", bytes.NewReader(b))
	if err != nil {
		return nil, err
	}
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", "Bearer "+obj.Key)
	resp, err := obj.Client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	bodyText, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	var v CreateImageStruct
	err = json.Unmarshal(bodyText, &v)
	if err != nil {
		return nil, err
	}
	return &v, nil
}

// ListModels 列出所有模型
func (obj *ChatAPI) ListModels() (*ListModelsStruct, error) {
	req, err := http.NewRequest("GET", "https://api.openai.com/v1/models", nil)
	if err != nil {
		return nil, err
	}
	req.Header.Set("Authorization", "Bearer "+obj.Key)
	resp, err := obj.Client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	bodyText, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	var v ListModelsStruct
	err = json.Unmarshal(bodyText, &v)
	if err != nil {
		return nil, err
	}
	return &v, nil
}
