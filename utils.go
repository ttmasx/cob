package cob

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"math/rand"
	"os"
	"time"
)

type Utils struct{}

var DUtils = &Utils{}

// GetRandomString 获取偶数位随机字符串
func (obj *Utils) GetRandomString(n int) (result string) {
	rand.Seed(time.Now().UnixNano())
	randBytes := make([]byte, n/2)
	rand.Read(randBytes)
	return fmt.Sprintf("%x", randBytes)
}

// GetRandomNum 获取[N,M]之间的随机整数，N<=M
func (obj *Utils) GetRandomNum(N, M int) (result int) {
	if M < N {
		log.Fatalln("N必须小于等于M!")
	}
	time.Sleep(time.Nanosecond)
	rand.Seed(time.Now().UnixNano())
	return rand.Intn(M-N+1) + N
}

// ReadFileToSlice 将文件中的所有非空行读入切片
func (obj *Utils) ReadFileToSlice(filepath string) (lines []string) {
	f, err := os.Open(filepath)
	if err != nil {
		log.Fatalln(err)
	}
	defer f.Close()
	reader := bufio.NewReader(f)
	for {
		line, _, err := reader.ReadLine()
		if err == io.EOF {
			break
		}
		if string(line) != "" {
			lines = append(lines, string(line))
		}
	}
	return
}
