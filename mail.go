package cob

import (
	"errors"
	"gopkg.in/gomail.v2"
	"strings"
)

// Mail
// Gmail https://www.codenong.com/cs105864356/ 1.开启两步验证 2.创建专用密码 3.选择customer类型
// Gmail 国内网络可能会超时
type Mail struct {
	Username string // 邮箱地址
	Password string // 邮箱授权码，非密码
}

func NewMail(username, password string) *Mail {
	return &Mail{
		Username: username,
		Password: password,
	}
}

func (obj *Mail) SendEmail(to string, subject string, body string) error {
	var host string
	var port int
	if strings.HasSuffix(obj.Username, "@gmail.com") {
		host = "smtp.gmail.com"
		port = 587
	} else if strings.HasSuffix(obj.Username, "@qq.com") {
		host = "smtp.qq.com"
		port = 465
	} else {
		return errors.New("不支持该邮箱！")
	}

	m := gomail.NewMessage()
	m.SetHeader("From", obj.Username)
	m.SetHeader("To", to)
	m.SetHeader("Subject", subject)
	m.SetBody("text/html", body)
	return gomail.NewDialer(host, port, obj.Username, obj.Password).DialAndSend(m)
}
