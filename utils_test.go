package cob

import (
	"fmt"
	"testing"
)

func TestUtils(t *testing.T) {
	fmt.Println(DUtils.GetRandomString(10))

	for i := 0; i < 50; i++ {
		fmt.Printf("%v ", DUtils.GetRandomNum(0, 9))
	}
	fmt.Println()

	lines := DUtils.ReadFileToSlice("1.txt")
	fmt.Println(len(lines), lines)
}
