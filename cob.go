package cob

import (
	"io"
	"log/slog"
	"os"
	"time"
)

func InitSlog() *slog.LevelVar {
	// 动态设置日志输出等级
	var programLevel = new(slog.LevelVar)
	programLevel.Set(slog.LevelDebug)

	// 设置日志输出
	f, err := os.OpenFile(time.Now().Format("2006-01-02")+".log", os.O_CREATE|os.O_RDWR|os.O_APPEND, 0644)
	if err != nil {
		panic(err)
	}

	// 开启行号记录
	h := slog.NewTextHandler(io.MultiWriter(os.Stdout, f), &slog.HandlerOptions{
		AddSource:   true,
		Level:       programLevel,
		ReplaceAttr: nil,
	})

	// 设置为系统默认日志
	slog.SetDefault(slog.New(h))
	return programLevel
}
