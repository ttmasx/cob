package cob

import (
	"crypto/hmac"
	"crypto/sha256"
	"encoding/base64"
	"fmt"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"time"
)

// DingTalk https://open.dingtalk.com/document/robots/customize-robot-security-settings
type DingTalk struct {
	Token  string // webhook中的token
	Secret string // 加签
}

func NewDingTalk(token string, secret string) *DingTalk {
	return &DingTalk{
		Token:  token,
		Secret: secret,
	}
}

// SendMsgToDingDing 钉钉机器人发送消息
func (obj *DingTalk) SendMsgToDingDing(msg string) error {
	timestamp, sign := obj.tokenToSign()
	u := fmt.Sprintf("https://oapi.dingtalk.com/robot/send?access_token=%v&timestamp=%v&sign=%v", obj.Token, timestamp, sign)
	var data = strings.NewReader(fmt.Sprintf(`{"msgtype": "text","text": {"content":"%v"}}`, msg))
	req, err := http.NewRequest("POST", u, data)
	if err != nil {
		return err
	}
	req.Header.Set("Content-Type", "application/json")
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	return nil
}

func (obj *DingTalk) tokenToSign() (string, string) {
	timestamp := strconv.FormatInt(time.Now().UnixNano()/int64(time.Millisecond), 10)
	secret_enc := []byte(obj.Secret)
	string_to_sign := fmt.Sprintf("%s\n%s", timestamp, obj.Secret)
	string_to_sign_enc := []byte(string_to_sign)
	h := hmac.New(sha256.New, secret_enc)
	h.Write(string_to_sign_enc)
	hmac_code := h.Sum(nil)
	sign := url.QueryEscape(base64.StdEncoding.EncodeToString(hmac_code))
	return timestamp, sign
}
